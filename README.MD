# FermentoPi thermal box inset

FDM 3D-printed wall inset for styrofoam box. Designed for a wall thickness of 30 mm. Adjust to your needs.

Provides mounting holes for a Raspberry Pi 3/4 as well as vertical venting channels.

One of the channels (exhaust) should be equipped with a 40mm PWM-controlled fan, e.g.
the Noctua NF-A4x10 5V PWM. The second, passive channel can be used as a cable feedthrough.

The vertical channels largely prevent air exchange unless driven by the fan (and the inside
is warmer than the outside air...).

Optionally, mounting channels for status LEDs are present.

For good insulation, use a sparse infill with lots of chambers between the perimeters.
Recommendations for Prusa Slicer / Slic3r:

  * Fill density: 10%
  * Fill pattern: Cubic

# License

This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
